function map(arrForTestMap, callBack) {
    if (!Array.isArray(arrForTestMap)) {
        console.log("Input data is not an Array");
    }
    let resultMap = [];
    for (let index = 0; index < arrForTestMap.length; index++) {
        resultMap.push(callBack(arrForTestMap[index], index, arrForTestMap));
    }
    return resultMap;
}
module.exports = map;