const filterByMe = require('../filter');

const arrForTestFilter = [1, 2, 3, 4, 5];

const returnArrayByFilterFunction = filterByMe(arrForTestFilter, (elementArray, index, arrFromFilterFunction) => {
    return elementArray % 2 === 0;
});

console.log(returnArrayByFilterFunction);
// console.log(typeof returnArrayByFilterFunction);