const each = require("../each");

let arrForTestEach = [1, 2, 3, 4, 5];

const toCheckReturnType = each(arrForTestEach, (arrElement, index, /*Optional --> Array Parameter */) => {
    // console.log(arrFromCallBack);
    console.log(`Element at index ${index} is ${arrElement}`);
});

console.log(typeof toCheckReturnType);