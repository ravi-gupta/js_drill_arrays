const reduce = require('../reduce');

let arrForTestReduce = [1, 2, 3, 4, 5];

let reduceAnswer = reduce(arrForTestReduce, (accumulator, currValue) => {
    return accumulator * currValue;
}, 1);

console.log(reduceAnswer);