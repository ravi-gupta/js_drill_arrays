const map = require('../map');

let arrForTestMap = [1, 2, 3, 4, 5];

const returnFromMapFunction = map(arrForTestMap, (elementFromArray, index,/*Optional Array from map function */) => {
    return elementFromArray * 2;
});

console.log(returnFromMapFunction);
//console.log(typeof returnFromMapFunction);