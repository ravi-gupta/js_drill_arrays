const findByMe = require('../find');

let arrForTestFind = [1, 2, 3, 4, 5];

const returnBYFindFunction = findByMe(arrForTestFind, (elementOfArray, index, arrFromFindFunction) => {
    return elementOfArray % 2 === 0;
});

console.log(returnBYFindFunction);