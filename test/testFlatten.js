const flattenByMe = require('../flatten');



const nestedArray = [1, [2], [[3]], [[[4]]]];
const depthByMe = Infinity;

const returnFromFlattenFunction = flattenByMe(nestedArray, depthByMe);

console.log(returnFromFlattenFunction);