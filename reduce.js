function reduce(arrForTestReduce, callBack, StartingValue) {
    if (!Array.isArray(arrForTestReduce)) {
        console.log('Input data is not an Array')
    }
    let accumulator = StartingValue !== undefined ? StartingValue : arrForTestReduce[0];
    let startingIndex = StartingValue !== undefined ? 0 : 1;

    for (let index = startingIndex; index < arrForTestReduce.length; index++) {
        accumulator = callBack(accumulator, arrForTestReduce[index], index, arrForTestReduce);
    }
    return accumulator;
}
module.exports = reduce;