function filterByMe(arrForTestFilter, callBack) {
    if (!Array.isArray(arrForTestFilter)) {
        console.log("Input data is not an Array");
    }
    let resultFilter = [];
    for (let index = 0; index < arrForTestFilter.length; index++) {
        if (callBack(arrForTestFilter[index], index, arrForTestFilter)) {
            resultFilter.push(arrForTestFilter[index]);
        }
    }
    return resultFilter;
}
module.exports = filterByMe;