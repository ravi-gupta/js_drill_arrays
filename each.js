function each(arrForTestEach, callBack) {
    if (!Array.isArray(arrForTestEach)) {
        console.log("Input data is not an Array");
    }
    for (let index = 0; index < arrForTestEach.length; index++) {
        callBack(arrForTestEach[index], index, arrForTestEach);
    }
    return undefined;
}
module.exports = each; 
