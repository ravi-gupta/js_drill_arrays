function find(arrForTestFind, callBack) {
    if (!Array.isArray(arrForTestFind)) {
        console.log("Input data is not an Array");
    }
    for (let index = 0; index < arrForTestFind.length; index++) {
        if (callBack(arrForTestFind[index], index, arrForTestFind)) {
            return arrForTestFind[index];
        }
    }
    return undefined;
}
module.exports = find;