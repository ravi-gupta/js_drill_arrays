function flattenByMe(nestedArray, depthByMe = 1) {
    if (!Array.isArray(nestedArray)) {
        console.log("Input data is not an Array");
        return [];
    }
    let setDepthByMe = depthByMe === Infinity ? Infinity : depthByMe;
    let arrFlatten = [];
    function flatten(arr, currDepth) {
        for (let index = 0; index < arr.length; index++) {
            let element = arr[index];
            if (Array.isArray(element) && currDepth < setDepthByMe) {
                currDepth++;
                flatten(element, currDepth);
            }
            else {
                arrFlatten.push(element);
            }
        }
    }
    flatten(nestedArray, 0);
    return arrFlatten;
}
module.exports = flattenByMe;
